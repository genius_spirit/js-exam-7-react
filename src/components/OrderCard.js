import React from 'react';

const OrderCard = ({orders, remove}) => {
  return(
     orders.map((item, index) => {
      if (item.orderPrice === 0) {
        return null;
      }
      return (
        <div className='orderItem' key={index}>
          <span className='type'>{item.type}</span>
          <span className='amount'>x {item.amount}</span>
          <span className='price'>{item.orderPrice}</span>
          <span className='cash'>KGS</span>
          <input type="submit" value='X' className='remove' onClick={() => remove(index)}/>
        </div>
      )
    })
  )
};

export default OrderCard;