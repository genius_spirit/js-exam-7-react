import React from 'react';
import './AddItems.css';

const AddItems = ({prices, click}) => {
  return(
    <div className="items">
        {prices.map((item, index) => {
          return (
            <div className="item" key={index} onClick={() => click(index)}>
              <div className='img'><img src={item.src} alt={item.type}/></div>
              <div className='itemTitle'>{item.type}</div>
              <div className='itemPrice'>Price: {item.price} KGS</div>
            </div>
          )
        })}
    </div>
  )
};

export default AddItems;