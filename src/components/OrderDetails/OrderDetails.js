import React from 'react';
import './OrderDetails.css';
import OrderCard from "../OrderCard";

const OrderDetails = ({orders, total, remove}) => {
  if (total === 0) {
    return (
      <div className='orders'>
        <div>Order is empty!</div>
        <div>Please add some items!</div>
      </div>
    )
  }
  return(
    <div className="orders" >
      <OrderCard orders={orders} remove={remove} />
      <div className='total'>Total price: {total} KGS</div>
    </div>
  )
};

export default OrderDetails;