import React, { Component } from 'react';
import './App.css';
import OrderDetails from "../components/OrderDetails/OrderDetails";
import AddItems from "../components/AddItems/AddItems";

class App extends Component {

  prices = [
    {type: 'Hamburger', price: 80, src: '/img/hamburger.png'},
    {type: 'Cheeseburger', price: 90, src: '/img/cheeseburger.png'},
    {type: 'Fries',price: 50, src: '/img/fries.png'},
    {type: 'Coffee',price: 70, src: '/img/coffee.png'},
    {type: 'Tea',price: 50, src: '/img/tea.png'},
    {type: 'Coca-cola', price: 40, src: '/img/cola.png'}
  ];

  state = {
    goods: [
      {type: 'Hamburger', amount: 0, orderPrice: 0},
      {type: 'Cheeseburger', amount: 0, orderPrice: 0},
      {type: 'Fries', amount: 0, orderPrice: 0},
      {type: 'Coffee', amount: 0, orderPrice: 0},
      {type: 'Tea', amount: 0, orderPrice: 0},
      {type: 'Coca-cola', amount: 0, orderPrice: 0}
    ],
    totalPrice: 0
  };

  addItemToOrder = (index) => {
    const prices = [...this.prices];
    const priceItem = {...prices[index]};
    const goods = [...this.state.goods];
    const item = {...goods[index]};
    item.amount++;
    item.orderPrice += priceItem.price;
    goods[index] = item;

    let totalPrice = this.state.totalPrice;
    totalPrice += prices[index].price;
    this.setState({goods, totalPrice});
  };

  removeItem = (index) => {
    const goods = [...this.state.goods];
    const item = {...goods[index]};
    item.amount = 0;
    item.orderPrice = 0;
    goods[index] = item;
    let totalPrice = this.state.totalPrice;
    totalPrice -= this.state.goods[index].orderPrice;
    this.setState({goods, totalPrice});
  };

  render() {
    return (
      <div className="App">
        <OrderDetails
          orders={this.state.goods}
          total={this.state.totalPrice}
          remove={this.removeItem}/>
        <AddItems prices={this.prices}  click={this.addItemToOrder}/>
      </div>
    );
  }
}

export default App;
